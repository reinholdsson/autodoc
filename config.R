#####################
### CONFIGURATION ###
#####################

# Default columns in html output
default_cols <- c(".ID", ".PROJECT", ".README", "AUTHOR", "INTRODUCTION", "LANGUAGE") # write NULL to include all columns

# Type of readme files to search for
lookup_files <- c("readme.txt", "read.me", "readme", "readme.1st", "readme.md")

# Path to search for readme files
search_path <- "/home/reinholdsson/Dropbox/projects/autodoc2/test"

# JavaScript files to be included in html
js_files <- c(
  "/home/reinholdsson/Dropbox/projects/autodoc2/media/js/jquery.js",
  "/home/reinholdsson/Dropbox/projects/autodoc2/media/js/jquery.dataTables.min.js",
  "/home/reinholdsson/Dropbox/projects/autodoc2/media/js/ColVis.min.js")

# CSS files to be included in html
css_files <- c(
  "/home/reinholdsson/Dropbox/projects/autodoc2/media/css/demo_page.css",
  "/home/reinholdsson/Dropbox/projects/autodoc2/media/css/demo_table.css")

# HTML template to be used
template_path <- "/home/reinholdsson/Dropbox/projects/autodoc2/media/html/template.html"

# HTML output file
html_path <- "/home/reinholdsson/Dropbox/projects/autodoc2/test/index.html"
