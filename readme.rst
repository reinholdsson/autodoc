AutoDoc
=======

Author
------
Thomas Reinholdsson <reinholdsson@gmail.com>


Introduction
-------------
This is a script to lookup project's readme files,
and then summarize their information in a structured
HTML table. 

The HTML file make use of DataTables (http://datatables.net/) 
for presenting a nice and simple user interface.


How to use
----------
1) In R run the following command to install necessary packages:

::

  install.packages("RJSONIO")

2) Setup all the parameters in config.R.

3) Edit the source path for the config file in main.R, e.g:

::
  
  source("/home/reinholdsson/autodoc/config.R")

4) Run the program:

::
  
  R CMD BATCH main.R


Todo
----
- Fix relative paths
- Add the possibility to have the introduction right below the title
